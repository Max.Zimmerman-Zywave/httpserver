﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace HttpServer.Controllers
{
    [Route("api/Test")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly ILogger _logger;

        public TestController(ILoggerFactory logger)
        {
            _logger = logger.CreateLogger("Test");
        }

        [HttpGet("Delay")]
        public async Task<IActionResult> Delay(int seconds)
        {
            var beforeDelay = DateTime.Now;
            await Task.Delay(seconds * 1000);
            var afterDelay = DateTime.Now;

            return Ok($"[{beforeDelay} : {afterDelay}]");
        }
    }
}
